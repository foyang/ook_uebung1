package org.hbrs.ooka.ws2020.uebung1.buchungssystem;

import org.hbrs.ooka.ws2020.uebung1.buchungssystem.interfaces.Hotelsuche;
import org.hbrs.ooka.ws2020.uebung1.buchungssystem.model.Hotel;

import java.util.List;

public class Execution {
    public static void main(String[] args) {
        HotelRetrieval hotelRetrieval = new HotelRetrieval();
        List<Hotel> hotels = hotelRetrieval.getHotels();
        for (Hotel hotel: hotels){
            System.out.println("HotelName: "+hotel.getName());
        }
        System.out.println(hotels.size());

        Hotel koblenzHotel = hotelRetrieval.getHotelByName("Koblenz");
        System.out.println("Hotel By Name: "+koblenzHotel.getName());
    }
}
