package org.hbrs.ooka.ws2020.uebung1.buchungssystem.interfaces;

import java.util.List;

public interface Caching {
    String cacheResult( String key, List<Object> value);
}
