package org.hbrs.ooka.ws2020.uebung1.buchungssystem.interfaces;

import org.hbrs.ooka.ws2020.uebung1.buchungssystem.model.Hotel;

import java.util.List;

public interface Hotelsuche {
    Hotel getHotelByName(String name);
    List<Hotel> getHotels();
    void openSession();
}
