package org.hbrs.ooka.ws2020.uebung1.buchungssystem;

import org.hbrs.ooka.ws2020.uebung1.buchungssystem.interfaces.Caching;
import org.hbrs.ooka.ws2020.uebung1.buchungssystem.interfaces.Hotelsuche;
import org.hbrs.ooka.ws2020.uebung1.buchungssystem.model.Hotel;

import java.util.ArrayList;
import java.util.List;


public class HotelRetrieval implements Hotelsuche, Caching {

    DBAccess dbAccess = new DBAccess();

    @Override
    public String cacheResult(String key, List<Object> value) {
        return null;
    }

    @Override
    public Hotel getHotelByName(String name) {
        for (Hotel hotel:getHotels()){
            if (hotel.getName().equals(name)){
                return hotel;
            }
        }
        return null;
    }

    @Override
    public List<Hotel> getHotels() {
        dbAccess.openConnection();
        List<Hotel> hotelList = new ArrayList<>();
        List<String> names = dbAccess.getObjects(3, "*");
        for (String hotelName: names){
            Hotel hotel = new Hotel();
            hotel.setName(hotelName);
            hotelList.add(hotel);
        }
        return hotelList;
    }

    @Override
    public void openSession() {
        dbAccess.openConnection();
    }
}
